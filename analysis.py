import pandas as pd 
import numpy as np
import dask.dataframe as dd
import pickle
import os

tag_list_path = "E:\\Mechademy\\Tag_list\\DECP Master Tag List.xlsx"
sheet_name = 'PI_tags_Master'

def df_tag_to_name(df, tag_list_path=tag_list_path):
    '''
    Convert all the PI tag column names to mechademy tag name for any dataframe.

    RETURNS: new df with column names changed from tag to name.
    '''
    columns = pd.Series(df.columns, name='Variable Tag')
    tag_name = pd.read_excel(tag_list_path, sheet_name = sheet_name, usecols = ['Variable Name', 'Variable Tag'], engine='openpyxl')
    tag_name = tag_name.merge(columns.to_frame(), on='Variable Tag')
    tag_dict = {}
    for name, tag in zip(tag_name['Variable Name'], tag_name['Variable Tag']):
        tag_dict[tag] = name
    df = df.rename(columns=tag_dict)
    return df


def name_from_taglist(cols, tag_list_path=tag_list_path):
    '''
    Get respective equipment names for a list of PI tags.

    RETURNS: Dictionary with tag:name pair.
    '''
    columns = pd.Series(cols, name='Variable Tag')
    tag_name = pd.read_excel(tag_list_path, sheet_name = sheet_name, usecols = ['Variable Name', 'Variable Tag'], engine='openpyxl')
    tag_name = tag_name.merge(columns.to_frame(), on='Variable Tag')
    tag_dict = {}
    for name, tag in zip(tag_name['Variable Name'], tag_name['Variable Tag']):
        tag_dict[tag] = name

    return tag_dict


def df_name_to_tag(df, tag_list_path=tag_list_path):
    '''
    Converts all the columns names of a machines equipments from their 
    respective equipment name to PI tags.

    RETURNS: new df with column names changed from name to tags.
    '''
    columns = pd.Series(df.columns, name='Variable Name')
    tag_name = pd.read_excel(tag_list_path, sheet_name = sheet_name, usecols = ['Variable Name', 'Variable Tag'], engine='openpyxl')
    tag_name = tag_name.merge(columns.to_frame(), on='Variable Name')
    tag_dict = {}
    for name, tag in zip(tag_name['Variable Name'], tag_name['Variable Tag']):
        tag_dict[name] = tag
    df = df.rename(columns=tag_dict)
    return df


def tag_from_namelist(cols, tag_list_path=tag_list_path):
    '''
    Get respective tag names for a list of equipment names.
    '''
    columns = pd.Series(cols, name='Variable Name')
    tag_name = pd.read_excel(tag_list_path, sheet_name = sheet_name, usecols = ['Variable Name', 'Variable Tag'], engine='openpyxl')
    tag_name = tag_name.merge(columns.to_frame(), on='Variable Name')
    tag_dict = {}
    
    for name, tag in zip(tag_name['Variable Name'], tag_name['Variable Tag']):
        tag_dict[name] = tag

    return list(tag_dict.values())


def create_dataframe(path='data/',col_pickel_path='', start = 0):
    '''A function to preapare a prob dataset for modelling,
        from the complete FGC dartaset. Start in this function is used to point out the  start index,
        as the dataset is very noisy before October 2018'''
       
    dask_data = df_tag_to_name(dd.read_csv(path))
    col_list = []

    # Loading column names for probe dataset
    if col_pickel_path:
        with open(col_pickel_path, 'rb') as file:
            col_list = pickle.load(file)
        df = dask_data[col_list].compute().reset_index(drop=True)
        df = df.iloc[start:, :].reset_index(drop=True)
        return df
    else:
        print('Please provide a columns list to continue with, or use pandas to load the whole csv')



def continuous_var_summary(x):
    '''
    Return a more detailed summary of a dataframe
    '''
    return pd.Series([x.count(), x.isnull().sum(), x.sum(), x.mean(), x.median(),  
                      x.std(), x.var(), x.min(), x.quantile(0.01), x.quantile(0.05),
                          x.quantile(0.10),x.quantile(0.25),x.quantile(0.50),x.quantile(0.75), 
                              x.quantile(0.90),x.quantile(0.95), x.quantile(0.99),x.max()], 
                  index = ['N', 'NMISS', 'SUM', 'MEAN','MEDIAN', 'STD', 'VAR', 'MIN', 'P1', 
                               'P5' ,'P10' ,'P25' ,'P50' ,'P75' ,'P90' ,'P95' ,'P99' ,'MAX'])


def run_creator(y_name, operation=''):
    '''
    Return a string with a structured name for identification of what 
    we are working on and what operation is being performed.
    '''
    a = y_name+'['+tag_from_namelist(y_name)[0]+']'+operation
    return a

import pandas as pd 
import numpy as np
import optuna
from sklearn.model_selection import train_test_split, cross_val_score
import matplotlib as mpl 
import mlflow
from optuna.visualization import plot_optimization_history
import xgboost as xgb
from bayes_opt import BayesianOptimization
mpl.rcParams['agg.path.chunksize'] = 10000
from Scripts import analysis

def hyper_tuner_optuna(X, y, exp_no, run_no):
    '''
    Hypertuning xgboost hyperparameters using optuna
    '''
    features = X.columns.values
    with mlflow.start_run(experiment_id=exp_no, run_name=analysis.run_creator(y.name, operation='(Hypertuning)')):
        mlflow.set_tag('Model.Run', run_no)
        
        x_train, x_test, y_train, y_test = train_test_split(X, y,train_size=0.4, test_size=0.25, random_state=35)

        def objective(trial, x_train, y_train, x_test, y_test):
            
            xg_n_estimators = trial.suggest_int("xg_n_estimators", 50, 150)
            xg_max_depth = trial.suggest_int("xg_max_depth", 5, 10)
            xg_learning_rate = trial.suggest_float("xg_learning_rate", 0.05, 0.15)
            xg_subsample = trial.suggest_float("xg_subsample", 0.6, 1.0)
            xg_colsample_bytree = trial.suggest_float("xg_colsample_bytree", 0.6, 1.0)
            xg_min_child_weight = trial.suggest_int('xg_min_child_weight',  4, 15)
            model=xgb.XGBRegressor(objective='reg:squarederror', max_depth = xg_max_depth, n_estimators= xg_n_estimators, \
                                learning_rate = xg_learning_rate, \
                                subsample = xg_subsample, colsample_bytree=xg_colsample_bytree, \
                                min_child_weight=xg_min_child_weight, tree_method='gpu_hist')
            
            score = -cross_val_score(model, x_train, y_train, cv=2, n_jobs=-1, scoring='neg_mean_absolute_error')
            mae = np.mean(score, dtype='float64')
            return mae

        
        study = optuna.create_study(direction='minimize',pruner=optuna.pruners.HyperbandPruner(max_resource="auto"))
        study.optimize(lambda trial: objective(trial, x_train, y_train, x_test, y_test), n_trials=15) 
        temp_df = study.trials_dataframe()

        for i in range(len(temp_df)):
            mlflow.start_run(experiment_id=exp_no, run_name='subrun {}'.format(i), nested=True)
            mlflow.log_param("max_depth", temp_df['params_xg_max_depth'][i])
            mlflow.log_param("n_estimators", temp_df['params_xg_n_estimators'][i])
            mlflow.log_param("learning_rate", temp_df['params_xg_learning_rate'][i])
            mlflow.log_param("subsample", temp_df['params_xg_subsample'][i])
            mlflow.log_param("colsample_bytree", temp_df['params_xg_colsample_bytree'][i])
            mlflow.log_metric("MAE", temp_df['value'][i])
            mlflow.end_run()
        
        mlflow.set_tag('feature_names', features)
        mlflow.log_param("best_max_depth", study.best_params['xg_max_depth'])
        mlflow.log_param("best_n_estimators", study.best_params['xg_n_estimators'])
        mlflow.log_param("best_learning_rate", study.best_params['xg_learning_rate'])
        mlflow.log_param("best_subsample", study.best_params['xg_subsample'])
        mlflow.log_param("best_colsample_bytree", study.best_params['xg_colsample_bytree'])
        mlflow.log_param("min_child_weight", study.best_params['xg_min_child_weight'])
        mlflow.log_metric("best_mae", study.best_value)

        
            
        plot = plot_optimization_history(study)
        plot.write_image(analysis.run_creator(y.name)+"/optuna_optimization_history.png")
        mlflow.log_artifact(analysis.run_creator(y.name)+"/optuna_optimization_history.png")
        
        plot = optuna.visualization.plot_param_importances(study)
        plot.write_image(analysis.run_creator(y.name)+"/optuna_param_imp.png")
        mlflow.log_artifact(analysis.run_creator(y.name)+"/optuna_param_imp.png")
        
        plot = optuna.visualization.plot_slice(study)
        plot.write_image(analysis.run_creator(y.name)+"/optuna_plot_slice.png")
        mlflow.log_artifact(analysis.run_creator(y.name)+"/optuna_plot_slice.png")
        
        plot = optuna.visualization.plot_parallel_coordinate(study)
        plot.write_image(analysis.run_creator(y.name)+"/optuna_parallel_coordinate.png")
        mlflow.log_artifact(analysis.run_creator(y.name)+"/optuna_parallel_coordinate.png")
    return study

def hypertuner_bayes(X, y, exp_no, run_no):
    '''
    Hypertuning a Xgboost model using bayesian optimization
    '''
    features = X.columns
    with mlflow.start_run(experiment_id=exp_no, run_name=analysis.run_creator(y.name, operation='(Hypertuning)')):
        mlflow.set_tag('Model.Run', run_no)
        
        x_train, x_test, y_train, y_test = train_test_split(X, y,train_size=0.4, test_size=0.25, random_state=35)
        def baye_opt(max_depth, min_child_weight, colsample_bytree, learning_rate, n_estimators, subsample):
            params_opt = {}
            params_opt['max_depth'] = int(max_depth)
            params_opt['min_child_weight'] = int(min_child_weight)
            params_opt['colsample_bytree'] = colsample_bytree
            params_opt['learning_rate'] = learning_rate
            params_opt['n_estimators'] = int(n_estimators)
            params_opt['subsample'] = subsample

            scores = cross_val_score(xgb.XGBRegressor(objective='reg:squarederror', tree_method='gpu_hist', **params_opt), x_train, y_train, scoring='neg_mean_absolute_error', cv=5).mean()
            return float(scores.mean())
        
        params ={'max_depth':(8, 10.9),
            'min_child_weight': (5, 13),
            'colsample_bytree':(0.6, 1),
            'learning_rate':(0.06, 0.11),
            'n_estimators':(100, 130),
            'subsample': (0.6, 1)}
        xgb_bo = BayesianOptimization(baye_opt, params)
        # Use the expected improvement acquisition function to handle negative numbers
        # Optimally needs quite a few more initiation points and number of iterations
        xgb_bo.maximize(init_points=6, n_iter=9, acq='ei')

        for i, res in enumerate(xgb_bo.res):
            mlflow.start_run(experiment_id=exp_no, run_name='subrun {}'.format(i), nested=True)
            mlflow.log_param("learning_rate", int(res['params']['learning_rate']))
            mlflow.log_param("max_depth", int(res['params']['max_depth']))
            mlflow.log_param("min_child_weight", int(res['params']['min_child_weight']))
            mlflow.log_param("n_estimators", res['params']['n_estimators'])
            mlflow.log_param("colsample_bytree", res['params']['colsample_bytree'])
            mlflow.log_param("subsample", res['params']['subsample'])
            mlflow.log_metric("MAE", res['target'])
            mlflow.end_run()
        
        mlflow.set_tag('feature_names', features)
        mlflow.log_param("best_max_depth", xgb_bo.max['params']['max_depth'])
        mlflow.log_param("best_n_estimators", xgb_bo.max['params']['n_estimators'])
        mlflow.log_param("best_learning_rate", xgb_bo.max['params']['learning_rate'])
        mlflow.log_param("best_subsample", xgb_bo.max['params']['subsample'])
        mlflow.log_param("best_colsample_bytree", xgb_bo.max['params']['colsample_bytree'])
        mlflow.log_param("min_child_weight", xgb_bo.max['params']['min_child_weight'])
        mlflow.log_metric("best_mae", xgb_bo.max['target'])

    return xgb_bo.max['params']



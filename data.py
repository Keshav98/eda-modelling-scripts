import boto3
import botocore

BUCKET_NAME = 'Enter Bucket Name'
KEY = 'Enter File path'

s3 = boto3.resource('s3')

try:
    s3.Bucket(BUCKET_NAME).download_file(KEY, "Insert_file_name_for_local")
except botocore.exceptions.ClientError as e:
    if e.response['Error']['Code'] == "404":
        print("The object does not exist.")
    else:
        raise
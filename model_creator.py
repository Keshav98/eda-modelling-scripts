import pandas as pd 
import numpy as np
import dask.dataframe as dd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.preprocessing import MinMaxScaler, StandardScaler, RobustScaler
import matplotlib.pyplot as plt 
import matplotlib as mpl 
import mlflow
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
import xgboost as xgb
from xgboost import plot_importance
from sklearn.pipeline import make_pipeline

import tensorflow as  tf
from tensorflow import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras  import layers
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers.experimental import preprocessing

mpl.rcParams['agg.path.chunksize'] = 10000
import pickle
import gc
import os
import zipfile

from Scripts import analysis


def eval_metrics(actual, pred):
    '''
    Returns model performance parameters.
    '''
    rmse = np.sqrt(mean_squared_error(actual, pred))
    mae = mean_absolute_error(actual, pred)
    r2 = r2_score(actual, pred)
    return rmse, mae, r2
 

def prediction_vs_actual(predicted_y, y_test):
    '''
    Plot actual vs predcited values from a model.
    '''
    plt.figure(figsize=(24,8))
    fig = plt.plot(y_test.values[::50])
    plt.plot(predicted_y[::50])
    plt.xlabel('Index')
    plt.ylabel('Vibrations')
    plt.title('PredictedvsActual')
    plt.legend(['Real', "Predicted"])
    plt.savefig(analysis.run_creator(y_test.name)+'/ActualvsPrediction.png', dpi=300)
    del fig
    plt.clf()
    gc.collect()


def plot_errors(perc, target):
    '''
    Plot percentage errors for a model.
    '''
    fig = plt.scatter(np.arange(len(perc)), perc, c='lime')
    plt.ylim(-20, 20)
    plt.xlabel('Points')
    plt.ylabel('Percentage error')
    plt.title('Percentage_error_plot')
    # plt.axis('tight')
    plt.savefig(analysis.run_creator(target)+'/Errors.png', dpi=300)
    del fig
    plt.clf()
    gc.collect()


def plot_xgb_feature_importance(model, target):
    '''
    Return feature importance plots for a xgb
    model using gain as importance threshold.
    '''
    fig = plot_importance(model, importance_type='gain')
    plt.tight_layout()
    plt.savefig(analysis.run_creator(target)+'/FeatureImportance(gain).png')
    del fig
    plt.clf()
    gc.collect()


def sample_model(x, y):
    '''
    Create a sample xgb model to feature selections using highly
    regularized hyper parameters.
    '''
    X_train, X_test, y_train, y_test = train_test_split(x, y, train_size=0.4, test_size=0.3, random_state = 123)
    dtrain = xgb.DMatrix(X_train, label=y_train)
    dtest = xgb.DMatrix(X_test, label=y_test)
    
    params = {
                # Parameters that we are going to tune.
                'max_depth':10,
                'min_child_weight': 8,
                'eta':.08,
                'objective':'reg:squarederror',
                'n_jobs' : 4,
                'subsample': 0.8,
                'colsample_bytree':0.9,
                'tree_method': 'gpu_hist',
                'gpu_id':0,
                'n_jobs':-1
    }
    params['eval_metric'] = "mae"
    
    model = xgb.train(
            params,
            dtrain,
            num_boost_round=200,
            evals=[(dtest, "Test")],
            early_stopping_rounds=10
            )
    return model, dtest


def get_feature_importance(model):
    '''
    Return importance of each feature in a model
    '''
    feature_important = model.get_score(importance_type='gain')
    keys = np.array(list(feature_important.keys()))
    values = np.array(list(feature_important.values()))
    data = pd.DataFrame(data=values, index=keys, columns=["score"]).sort_values(by = "score", ascending=False)
    return data


def final_model_training(X, y, study):
    '''
    Creates xgboost model using an optuna study
    '''
    x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.4, random_state=35)
    final_model = xgb.XGBRegressor( 
                    max_depth = study.best_params['xg_max_depth'],
                    min_child_weight = study.best_params['xg_min_child_weight'],
                    learning_rate = study.best_params['xg_learning_rate'],
                    n_jobs=-1,
                    subsample = study.best_params['xg_subsample'],
                    colsample_bytree=study.best_params['xg_colsample_bytree'],
                    objective='reg:squarederror',
                    n_estimators=study.best_params['xg_n_estimators'],
                    tree_method='hist')

    eval_set = [(x_test, y_test)]
    final_model.fit(x_train, y_train, early_stopping_rounds=10, eval_metric="mae", eval_set=eval_set, verbose=True)
    return final_model, x_test, y_test


def log_xgb_best_params(study):
    '''
    Logging all the model parameters for a xgboost model to mlflow
    '''
    mlflow.log_param("best_max_depth", study.best_params['xg_max_depth'])
    mlflow.log_param("best_n_estimators", study.best_params['xg_n_estimators'])
    mlflow.log_param("best_learning_rate", study.best_params['xg_learning_rate'])
    mlflow.log_param("best_subsample", study.best_params['xg_subsample'])
    mlflow.log_param("best_colsample_bytree", study.best_params['xg_colsample_bytree'])
    mlflow.log_param("min_child_weight", study.best_params['xg_min_child_weight'])
    return


def log_model_performance_metrics(rmse, r2, mae):
    '''
    Logging model performance parameters to mlflow
    '''
    mlflow.log_metric("rmse", rmse)
    mlflow.log_metric("r2", r2)
    mlflow.log_metric("mae", mae)
    return


def log_graphs(target):
    mlflow.log_artifact(analysis.run_creator(target)+'/ActualvsPrediction.png')
    mlflow.log_artifact(analysis.run_creator(target)+"/Errors.png")
    return

def zip_directory(folder_path, zip_path, target_name):
    '''
    Create a zipfile out of a folder.
    '''
    with zipfile.ZipFile(zip_path, mode='w') as zipf:
        len_dir_path = len(folder_path)
        for root, _, files in os.walk(folder_path):
            for file in files:
                file_path = os.path.join(root, file)
                # 2nd parameter in .write should be chosen wisely
                # example: if the full path of folder to be zipped is
                # target_name/model_files, then only model_files should be
                # passed in 2nd argument, to create a zip of only model files
                # Otherwise a rooted zip with first folder being target name
                # inside that model_files folder and inside that
                # our content will be created.
                zipf.write(file_path, file_path[len(analysis.run_creator(target_name)):])


def get_final_model_xgboost(X, y, run_no, exp, study):
    '''
    Creates and logs an xgboost model to mlflow after hypertuning 
    is finished.
    '''
    target = y.name
    with mlflow.start_run(experiment_id=exp, run_name=analysis.run_creator(y.name, '(Final_model)')):
        mlflow.set_tag('Model.Run', run_no)

        # Training model and logging features
        model, x_test, y_test = final_model_training(X, y, study)
        features = model.get_booster().feature_names
        mlflow.set_tag('Features', features)
        pickle.dump(model ,open(analysis.run_creator(y.name)+'/' +analysis.run_creator(y.name)+'.pkl', 'wb'))

        # Getting predcitions on test data and logging metrics
        predicted = model.predict(x_test)
        (rmse, mae, r2) = eval_metrics(y_test.values, predicted)
        print("  RMSE: %s" % rmse)
        print("  MAE: %s" % mae)
        print("  R2: %s" % r2)
        log_model_performance_metrics(rmse, r2, mae)

        percentage_error = ((y_test.values-predicted)/y_test.values)*100

        #Plotting model insights and logging on mlflow
        plot_errors(percentage_error, target)
        prediction_vs_actual(predicted, y_test)
        plot_xgb_feature_importance(model, target)
        log_graphs(target)
        
        # Logging Model on mlflow
        mlflow.xgboost.log_model(model, "model")
        mlflow.log_artifact(analysis.run_creator(target)+'/' +analysis.run_creator(target)+'.pkl')
    
    return model


def get_final_model_ridge(X, y, run_no, exp):
    '''
    Creates and logs an Ridge model to mlflow after hypertuning 
    is finished.
    '''
    target = y.name
    features = X.columns.values
    with mlflow.start_run(experiment_id=exp, run_name=analysis.run_creator(y.name, '(Final_model)')):
        mlflow.set_tag('Model.Run', run_no)

        # Splitting Data
        x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=35)

        # Training model and logging features
        model = make_pipeline(PolynomialFeatures(2 , interaction_only=False, include_bias=True), StandardScaler(), Ridge(alpha=0.1))
        model.fit(x_train, y_train)
        mlflow.set_tag('Features', features)
        pickle.dump(model ,open(analysis.run_creator(target)+'/' +analysis.run_creator(target)+'.pkl', 'wb'))

        # Getting predcitions on test data and logging metrics
        predicted = model.predict(x_test)
        (rmse, mae, r2) = eval_metrics(y_test.values, predicted)
        print("  RMSE: %s" % rmse)
        print("  MAE: %s" % mae)
        print("  R2: %s" % r2)
        log_model_performance_metrics(rmse, r2, mae)

        percentage_error = ((y_test.values-predicted)/y_test.values)*100

        #Plotting model insights and logging on mlflow
        plot_errors(percentage_error, target)
        prediction_vs_actual(predicted, y_test)
        log_graphs(target)
        
        # Logging Model on mlflow
        mlflow.sklearn.log_model(model, "model")
        mlflow.log_artifact(analysis.run_creator(target)+'/' +analysis.run_creator(target)+'.pkl')
    
    return model

def plot_loss(history, target):
    '''
    Create a training vs validation loss graph
    '''
    plt.plot(history.history['loss'], label='loss')
    plt.plot(history.history['val_loss'], label='val_loss')
    plt.xlabel('Epoch')
    plt.ylabel('Error [Vibration]')
    plt.legend()
    plt.grid(True)
    plt.savefig(analysis.run_creator(target)+'/TrainvsVal.png', dpi=300)
  

def get_final_model_keras(X, y, run_no, experiment_id, input_shape, dense_1=64, dense_2=128, dense_3=128, batch_size=64):
    '''
    Creates and logs an keras model to mlflow after hypertuning 
    is finished.
    '''
    target = y.name
    features = X.columns.values

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.35)

    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)

    os.mkdir(os.path.join(analysis.run_creator(y.name), 'model_files'))
    pickle.dump(scaler, open(os.path.join(os.path.join(analysis.run_creator(y.name), 'model_files'), 'scaler.pkl'), 'wb'))

    lr_schedule = tf.keras.optimizers.schedules.InverseTimeDecay(
              0.0009,
              decay_steps=(X_train.shape[0]/batch_size)*10,
              decay_rate=1,
              staircase=False)

    def get_optimizer():
        return tf.keras.optimizers.Adam(lr_schedule)

    model = Sequential([
                #  preprocessing.Normalization(),
                 layers.Dense(dense_1, input_shape=(input_shape, ), activation='relu'),
                #  layers.Dropout(0.2),
                 layers.Dense(dense_2, activation='relu'),
                 layers.Dense(dense_3, activation='relu'),
                #  layers.Dropout(0.2),
                 layers.Dense(1, activation='linear')            
                ]
                )

    loss = keras.losses.MeanAbsoluteError()

    model.compile(optimizer=get_optimizer(), loss=loss)


    with mlflow.start_run(experiment_id=experiment_id, run_name=analysis.run_creator(y.name, '(Keras_Final_model)')):
        mlflow.set_tag('Model.Run', run_no)
    
        mlflow.set_tag('Features', features)
        mlflow.tensorflow.autolog()
        history = model.fit(X_train, y_train, batch_size=batch_size, epochs=40, validation_split=0.2)
        plot_loss(history, y.name)

        y_pred = model.predict(scaler.transform(X_test)).reshape(-1)
        percentage_error = ((y_test.values-y_pred)/y_test.values)*100
        plot_errors(percentage_error)

        prediction_vs_actual(y_pred, y_test)

        model.save(os.path.join(analysis.run_creator(y.name),'model_files', 'model.h5'))

        zip_directory(analysis.run_creator(y_test.name)+'/model_files', analysis.run_creator(y_test.name)+'/model_files.zip', y.name)

        

        mlflow.log_artifact(analysis.run_creator(target)+'/ActualvsPrediction.png')
        mlflow.log_artifact(analysis.run_creator(target)+"/Errors.png")
        mlflow.log_artifact(analysis.run_creator(target)+"/TrainvsVal.png")

        mlflow.log_artifact(analysis.run_creator(target)+"/model_files.zip")